---
- name: Configure signing key for prosody repository
  apt_key:
    id: 107D65A0A148C237FDF00AB47393D7E674D9DBB5
    data: "{{ lookup('file', 'prosody-debian-packages.asc') }}"
    state: present

- name: Install Prosody Apt Repo
  apt_repository:
    repo: "deb https://packages.prosody.im/debian {{ ansible_distribution_release }} main"
    state: present
    filename: /etc/apt/sources.list.d/prosody

- name: Pin prosody version
  copy:
    dest: /etc/apt/preferences.d/prosody
    content: |
      Package: prosody
      Pin: version {{ jitsi_prosody_version }}
      Pin-Priority: 1002

      Package: prosody
      Pin: origin "packages.prosody.im"
      Pin-Priority: 1001


- name: Install Prosody # noqa 403
  apt:
    name: prosody
    state: latest
    update_cache: true
    cache_valid_time: 3600
  when: not videobridge_only

- name: copy global prosdoy config temmplate
  template:
    src: prosody_global_config.j2
    dest: "/etc/prosody/prosody.cfg.lua"
    owner: root
    group: root
    mode: "0644"
    validate: "luac -p %s"
  notify: restart prosody

- name: Copy Prosody config template.
  template:
    src: prosody_config.j2
    dest: "/etc/prosody/conf.avail/{{ jitsi_fqdn }}.cfg.lua"
    owner: root
    group: root
    mode: "0644"
    # Simply syntax check for validation, doesn't catch config errors.
    validate: "luac -p %s"
  notify: restart prosody

- name: Symlink Prosody config by domain name.
  file:
    path: "/etc/prosody/conf.d/{{ jitsi_fqdn }}.cfg.lua"
    state: link
    src: "/etc/prosody/conf.avail/{{ jitsi_fqdn }}.cfg.lua"
  notify: restart prosody

- name: Set jvb user file
  set_fact:
    # Yes, prosody actually URL-escapes the directory name for some reason.
    # Must hardcode the escaping in the prefix, since the replace filter only
    # applies to the server name var, not the concatenated string.
    jvb_user_file: /var/lib/prosody/{{ 'auth%2e'+jitsi_fqdn | replace('.', '%2e') |  replace('-', '%2d') }}/accounts/jvb.dat
    focus_user_file: /var/lib/prosody/{{ 'auth%2e'+jitsi_fqdn | replace('.', '%2e') | replace('-', '%2d') }}/accounts/focus.dat


- name: Register jicofo agent with Prosody service.
  command: >
    prosodyctl register focus auth.{{ jitsi_fqdn }} {{ jitsi_meet_jicofo_password }}
  args:
    # Yes, prosody actually URL-escapes the directory name for some reason.
    # Must hardcode the escaping in the prefix, since the replace filter only
    # applies to the server name var, not the concatenated string.
    creates: "{{ focus_user_file }}"
  notify:
    - restart jicofo
    - restart prosody

- name: Overwrite focus password
  template:
    src: focus.dat.j2
    dest: "{{ focus_user_file }}"
    owner: prosody
    group: prosody
    mode: '0640'
  notify:
    - restart jicofo
    - restart prosody

- name: Check user file exists
  stat:
    path: "{{ jvb_user_file }}"
  register: user_file

- name: Register jvb agent with Prosody service.
  command: >
    prosodyctl register jvb auth.{{ jitsi_fqdn }} {{ jitsi_meet_videobridge_password }}
  args:
    creates: "{{ jvb_user_file }}"
  when: not user_file.stat.exists
  notify:
    - restart jicofo
    - restart prosody

- name: Overwrite jvb password
  template:
    src: jvb.dat.j2
    dest: "{{ jvb_user_file }}"
    owner: prosody
    group: prosody
    mode: '0640'
  notify:
    - restart jicofo
    - restart prosody

- name: Generate SSL keypair for Prosody service.
  shell: >
    set -o pipefail && yes '' | prosodyctl cert generate {{ jitsi_fqdn }}
  args:
    creates: /var/lib/prosody/{{ jitsi_fqdn }}.crt
  notify:
    - restart jicofo
    - restart prosody

- name: Generate SSL keypair for Prosody auth service. # noqa 301
  shell: >
    set -o pipefail && yes '' | prosodyctl cert generate auth.{{ jitsi_fqdn }}
  args:
    creates: /var/lib/prosody/auth.{{ jitsi_fqdn }}.crt
  register: auth_cert_generated
  notify:
    - restart jicofo
    - restart prosody

- name: set file mode on certificates
  file:
    path: "/var/lib/prosody/{{ jitsi_fqdn }}.crt"
    owner: root
    group: prosody
    mode: '0640'
  with_items:
    - /var/lib/prosody/{{ jitsi_fqdn }}.crt
    - /var/lib/prosody/{{ jitsi_fqdn }}.key
    - /var/lib/prosody/auth.{{ jitsi_fqdn }}.crt
    - /var/lib/prosody/auth.{{ jitsi_fqdn }}.key

- name: Assure localhost ssl cert mode/owner
  file:
    path: "{{ item }}"
    owner: root
    group: prosody
    mode: '0644'
  loop:
    - /etc/prosody/certs/localhost.key
    - /etc/prosody/certs/localhost.crt
    - /etc/prosody/certs/localhost.cnf

- name: symlink prosody certs
  file:
    src: "/var/lib/prosody/{{ jitsi_fqdn }}.crt"
    dest: "/etc/prosody/certs/{{ jitsi_fqdn }}.crt"
    state: link
  notify: restart prosody

- name: symlink prosody key
  file:
    src: "/var/lib/prosody/{{ jitsi_fqdn }}.key"
    dest: "/etc/prosody/certs/{{ jitsi_fqdn }}.key"
    state: link
  notify: restart prosody

- name: symlink prosody auth certs
  file:
    src: "/var/lib/prosody/auth.{{ jitsi_fqdn }}.crt"
    dest: "/etc/prosody/certs/auth.{{ jitsi_fqdn }}.crt"
    state: link
  notify: restart prosody

- name: symlink prosody auth key
  file:
    src: "/var/lib/prosody/auth.{{ jitsi_fqdn }}.key"
    dest: "/etc/prosody/certs/auth.{{ jitsi_fqdn }}.key"
    state: link
  notify: restart prosody

- name: update-ca-certificates
  command: update-ca-certificates -f
  when: auth_cert_generated.changed # noqa 503

# Section to support jibri
- name: Register jibri agent with Prosody service.
  command: >
    prosodyctl register jibri auth.{{ jitsi_fqdn }} {{ jibri_user_jibri_password }}
  args:
    # Yes, prosody actually URL-escapes the directory name for some reason.
    # Must hardcode the escaping in the prefix, since the replace filter only
    # applies to the server name var, not the concatenated string.
    creates: /var/lib/prosody/{{ 'auth%2e'+jitsi_fqdn | replace('.', '%2e') | replace('-', '%2d') }}/accounts/jibri.dat
  when: jibri_enable
  notify:
    - restart jicofo
    - restart prosody

- name: Register recorder agent with Prosody service.
  command: >
    prosodyctl register recorder recorder.{{ jitsi_fqdn }} {{ jibri_user_recorder_password }}
  args:
    # Yes, prosody actually URL-escapes the directory name for some reason.
    # Must hardcode the escaping in the prefix, since the replace filter only
    # applies to the server name var, not the concatenated string.
    creates: /var/lib/prosody/{{ 'recorder%2e'+jitsi_fqdn | replace('.', '%2e') | replace('-', '%2d') }}/accounts/recorder.dat
  when: jibri_enable
  notify:
    - restart jicofo
    - restart prosody

- name: cron to restart Prosody nightly
  cron:
    name: restart prosody
    hour: "3"
    minute: "55"
    user: root
    job: "/usr/bin/systemctl restart prosody >> /var/log/jitsi/restart_prososdy.log 2>&1"
    cron_file: restart_prosody
  when: not videobridge_only
